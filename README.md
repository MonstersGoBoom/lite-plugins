Plugins for the [lite text editor](https://github.com/rxi/lite)

*Note: if you make a pull request, the table should be kept in alphabetical order*

---

Plugin | Description
-------|-----------------------------------------
[`language_6502`](language_6502.lua?raw=1) | Syntax for 6502/65C02 Assembly language
[`language_fe`](language_fe.lua?raw=1) | Syntax for the [fe](https://github.com/rxi/fe) programming language
[`language_odin`](language_odin.lua?raw=1) | Syntax for the [Odin](https://github.com/odin-lang/Odin) programming language
[`macmodkeys`](macmodkeys.lua?raw=1) | Remaps mac modkeys `command/option` to `ctrl/alt`
[`togglesnakecamel`](togglesnakecamel.lua?raw=1) | Toggles symbols between snake\_case and camelCase

